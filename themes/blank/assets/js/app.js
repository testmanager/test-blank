var App = function() {
    var $header = $('.navbar-autohide'),
        scrolling = false,
        previousTop = 0,
        scrollDelta = 10,
        scrollOffset = 150;

    /**
     * Initialize tooltips globally
     */
    var initTooltips = function(){
        $(document).tooltip({
            selector: "[data-toggle=tooltip]"
        });
    };

    /**
     * Listen page scrolling and hide/show navbar
     */
    var initScrollListener = function() {
        $(window).on('scroll', function(){
            if (!scrolling) {
                scrolling = true;

                if (!window.requestAnimationFrame) {
                    setTimeout(App.hideHeader, 250);
                }
                else {
                    requestAnimationFrame(App.hideHeader);
                }
            }
        });
    };

    /**
     * Hide/show navbar
     */
    var hideHeader = function() {
        var currentTop = $(window).scrollTop();

        // Scrolling up
        if (previousTop - currentTop > scrollDelta) {
            $header.removeClass('is-hidden');
        }
        else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
            // Scrolling down
            $header.addClass('is-hidden');
        }

        previousTop = currentTop;
        scrolling = false;
    };

    return {
        init: function () {
            initTooltips();
            initScrollListener();
        },
        hideHeader: hideHeader
    };
}();

// Initialize when page loads
jQuery(function(){
    App.init();
});
