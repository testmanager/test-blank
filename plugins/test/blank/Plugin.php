<?php namespace Test\Blank;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Test Blank',
            'description' => 'Blank for test job.',
            'icon'        => 'icon-puzzle-piece'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Test\Blank\Components\Todo' => 'todo'
        ];
    }
}
