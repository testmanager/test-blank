<?php namespace Test\Blank\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use October\Rain\Exception\ApplicationException;

class Todo extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Todo List',
            'description' => 'Implements a simple to-do list.'
        ];
    }

    public function defineProperties()
    {
        return [
            'max' => [
                'description'       => 'The most amount of todo items allowed',
                'title'             => 'Max items',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items value is required and should be integer.'
            ]
        ];
    }

    /**
     * This code will be executed when the page or layout is
     * loaded and the component is attached to it.
     */
    public function onRun()
    {
        $this->addJs('/plugins/test/blank/components/todo/assets/js/todo.js');

        $this->page['items'] = Session::get('items', []);
    }

    /**
     * Add new item
     * @throws ApplicationException
     */
    public function onAddItem()
    {
        $items = Session::get('items', []);

        if (count($items) >= $this->property('max')) {
            throw new ApplicationException(sprintf('Sorry only %s items are allowed.', $this->property('max')));
        }

        if (($newItem = post('newItem')) != '') {
            $items[] = $newItem;
        }

        Session::put('items', $items);
        $this->page['items'] = $items;
    }

    /**
     * Remove item
     */
    public function onRemoveItem()
    {
        $items = Session::get('items', []);
        $id = post('id');

        if(!is_null($id) && isset($items[$id])) {
            unset($items[$id]);
        }

        Session::put('items', $items);
        $this->page['items'] = $items;

        return [
            '.js-result' => $this->renderPartial('todo::list')
        ];
    }
}
