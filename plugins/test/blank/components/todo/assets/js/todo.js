var ToDo = function() {

    /**
     * Initialize item remove button
     */
    var initItemRemoveButton = function(){
        $('.js-result').on('click', '.js-todo_remove', function(){
            $.request('onRemoveItem', {
                data: {
                    id: $(this).data('id')
                }
            });
        });
    };

    return {
        init: function () {
            initItemRemoveButton();
        }
    };
}();

// Initialize when page loads
jQuery(function(){
    ToDo.init();
});
