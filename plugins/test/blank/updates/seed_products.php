<?php namespace Test\Blank\Updates;

use October\Rain\Database\Updates\Seeder;
use Schema;
use Test\Blank\Models\Product;

class SeedProducts extends Seeder
{

    public function run()
    {
        $products = [
            [
                'name' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, ex.',
                'price' => 20,
                'rate' => 1
            ],
            [
                'name' => 'Product Two',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis facere hic quos. Atque consectetur culpa eveniet nisi praesentium qui tenetur.',
                'price' => 28,
                'rate' => 5
            ],
            [
                'name' => 'Product Three',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum explicabo impedit ipsam odit quod tempora vero. Dolore eaque enim fuga iusto placeat quis sapiente suscipit? Consequuntur cumque dolorem in quam?',
                'price' => 45,
                'rate' => 3
            ],
            [
                'name' => 'Product Four',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequuntur deserunt, dicta, dolor eligendi et ex facere fuga fugit illo ipsam minus mollitia praesentium quisquam, tempora unde vel. Atque exercitationem incidunt odit voluptatem. Dolore dolorem laboriosam nostrum officia quos ut?',
                'price' => 72,
                'rate' => 2
            ],
            [
                'name' => 'Product Five',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores autem commodi corporis culpa, dignissimos dolorem doloremque dolorum error est exercitationem impedit non omnis perferendis possimus praesentium provident reiciendis sequi veritatis. A accusamus ad, doloremque explicabo impedit iste labore laboriosam nostrum nulla numquam omnis perferendis provident, voluptatem. Accusamus cupiditate dolorem facilis.',
                'price' => 15,
                'rate' => 5
            ],
        ];

        Product::insert($products);
    }
}
